<?php 
$aa=$_GET['brandTitleId'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/common.css">
    <link rel="stylesheet" href="../css/distop.css">
    <title>Document</title>
</head>

<body>
    <div id="ctl00_header1" class="header1 clearfix1">
        <div class="fl logo">
            <a href="javascript:;" title="慢慢买比价网--手机版">
                <img src="../images/header_logo.png" alt="慢慢买比价网--手机版">
            </a>
        </div>
        <div class="fr appdown">
            <a href="http://m.manmanbuy.com/app.html?type=headword&value=wap" onclick="trackEvent('header下载');" title="手机app下载">
                <img src="https://img.alicdn.com/imgextra/i2/690311/TB2beKvaTXYBeNkHFrdXXciuVXa_!!690311.png" alt="手机app下载">
            </a>
        </div>
    </div>
    <form action="#" method="get" name="frm">
        <div id="ctl00_searchmmb" class="search1 clearfix1">
            <div class="searchtxt">
                <input value="" name="key" type="search" placeholder="请输入你想比价的商品">
            </div>
            <div class="searchbtn">
                <input type="submit" value="搜索">
            </div>
        </div>
    </form>
    <div class="nav">
        <a href="http://m.manmanbuy.com" class="f12">首页</a> >
        <a href="top10.aspx" class="f12">
            品牌大全
        </a>
        <a class="top10"></a>
    </div>
    <div class="model" style="background-color:#fff;">
        <div class="hd">
            <h3>
                <span></span>哪个牌子好</h3>
        </div>
        <div class="bd sortlist">
            <ul>

            </ul>
        </div>
    </div>
    <div class="model" style="background-color:#fff;">
        <div class="hd">
            <h3>
                <span></span>销量排行</h3>
        </div>
        <div class="bd prolist">
            <ul>

            </ul>
        </div>
    </div>

    <div class="model" style="background-color:#fff;">
        <div class="hd">
            <h3>
                <span></span>最有用的用户评论</h3>
        </div>
        <div class="bd protit">
            <ul></ul>
        </div>
    </div>
    <div class="fooler">
        <div class="fool-top">
            <a href="javascript:;">登录</a>
            <a href="javascript:;">注册</a>
            <a href="#ctl00_header1">↑返回顶部</a>
        </div>
        <div class="fool-bot">
            <p>
                <a href="">手机APP下载</a>
                <a href="">慢慢买手机版</a>
                -- 掌上比价平台
            </p>
            <p>m.manmanbuy.com</p>
        </div>
    </div>
</body>

</html>
<script src="../js/zepto.js"></script>
<script src="../js/template-web.js"></script>
<script type="text/template" id="temp">
    {{each data2 value}}
    <li>
        <a href='../prolist/prolist.php?id=4&ppid={{value.brandId}}' title='{{value.brandName}}'>
            <em class='top top{{value.index}}'>{{value.index}}</em>
            <div class="inner">
                <div class="tit">{{value.brandName}}</div>
                <p>{{value.brandInfo}}</p>
            </div>
        </a>
    </li>
    {{/each}}
</script>
<script type="text/template" id="prolist">
    {{each data value}}
    <li>
        <a href='bijia.aspx?id=267556' title="{{value.productName}}">
            <div class='pic'>
            </div>
            <div class="info">
                <div class="tit">{{value.productName}}</div>
                <div class="price">
                    <em>{{value.productPrice}}</em>
                    <span class="star">
                        <em class="star5"></em>
                    </span>
                </div>
                <div class="other">
                    <em>{{value.productQuote}}</em>
                    <em>{{value.productCom}}</em>
                </div>
            </div>
        </a>
    </li>
    {{/each}}
</script>
<script type="text/template" id="protit">
    {{each data value}}
    <li>
        <a href='bijia.aspx?id=303821'>
            <div class='protit'>
                <div class='pic'>
                </div>
                <div class='tit'></div>
            </div>
        </a>
        <div class='plbox'>
            <a href='discomment.aspx?id={{value.productId}}&cid={{value.comId}}'>
                <div class='name'>{{value.comName}}:
                    <span class='star'>
                        <em class='star5'>☆☆☆☆☆</em>
                    </span>
                    <i>{{value.comTime}}</i>
                </div>
                <div class='con'>{{value.comContent}}</div>
        </div>
        </a>
    </li>
    {{/each}}
</script>
<script>
    var top11 = "<?php $bb=$_GET['title']; echo $bb?>"
    var New = top11.split("十")
    var Productid;
    for (i = 0; i < $('.hd').length; i++) {
        $('.hd span').eq(i).html(New[0])
    }
    var brandtitleid = <?php echo $aa?>;
    $.get("http://193.112.55.79:9090/api/getbrand", {
        "brandtitleid": brandtitleid
    }, function(res) {
        // console.log(res);
        var data = res.result;
        var arr2 = []
        // 切割字符串，获取销量数据
        for (i = 0; i < data.length; i++) {
            var info = data[i].brandInfo;
            arr = info.split("：")
            var xiaoliang = parseInt(arr[1])
            arr2.push(xiaoliang)
        }
        // 根据销量对获取到的数据排序
        for (j = 0; j < arr2.length - 1; j++) {
            for (var i = 0; i < arr2.length - 1 - j; i++) {
                if (arr2[i] > arr2[i + 1]) {
                    var temp = arr2[i];
                    arr2[i] = arr2[i + 1];
                    arr2[i + 1] = temp;
                    var jimi = data[i];
                    data[i] = data[i + 1];
                    data[i + 1] = jimi
                }
            }
        }
        var data2 = data.reverse()

        for (i = 0; i < data2.length; i++) {
            data2[i].index = i + 1
        }
        $(".sortlist ul").html(template('temp', {"data2": data2}));

    }, 'json');
    $.get("http://193.112.55.79:9090/api/getbrandproductlist", {
        "brandtitleid": brandtitleid
    }, function(res) {
        if (res) {
            var data = res.result.slice(0, 4);
            if (data[0]) {
                Productid = data[0].productId;
            } else {
                Productid = 2
            }
            $(".prolist ul").html(template('prolist', {
                "data": data
            }));
            //   将获取的图片标签基地址一并写入结构中 
            var pic = data[0];
            for (i = 0; i < data.length; i++) {
                $('.pic').eq(i).append(data[i].productImg)
            }
        }
        // 发送ajax请求评论数据
        $.get("http://193.112.55.79:9090/api/getproductcom", {
            "productid": Productid
        }, function(res) {
            var data = res.result;
            // 拿到评论的数据，渲染到页面
            $(".protit ul").html(template('protit', {
                "data": data
            }));
            // console.log( $('.protit .pic'));
            // console.log(pic);
            for (i = 0; i < data.length; i++) {
                $('.protit .pic').eq(i).append(pic.productImg);
                $('.protit .tit').eq(i).html(pic.productName);
            }
        }, 'json');
    }, 'json');
    console.log(location.search);
</script>