$(function () {
  init();

  function init() {
    // 就需要执行的代码
    getIndexmenu();

    // 对于一些动态生成的标签 绑定事件
    // 1 使用委托
    // 2 帮绑定的代码 放在发送请求的成功的回调函数里面
    // evnetList();
  }

  // 绑定一坨事件
  // function () {
  //   // 点击更多 切换显示后四个菜单
  //   // console.log( $("nav a:nth-child(8)"));
  //   // 事件委托
  //   $("nav").on("tap", " a:nth-child(8)", function () {
  //     // console.log(123);

  //     $("nav a:nth-last-child(-n+4)").toggle(500);
  //     // $("nav a:nth-last-child(-n+4)").fadeToggle(500);
  //     // $("nav a:nth-last-child(-n+4)").slideToggle(500);
  //   })

  // }

  // 获取首页导航菜单的数据
  function getIndexmenu() {
    // 使用zepto发送请求
    $.get("http://193.112.55.79:9090/api/getsitenav", function (ret) {
      // console.log(ret);
      // template(模板的id,要渲染的数据)
      var html = template("navTpl", { arr: ret.result });
      // console.log(html);
      // 渲染
      $(".nav_content").html(html);

      // evnetList();
    })
  }
})
