window.onload=function () {
    //点击回到顶部--底部功能
  $("#callTop").on("click",function(){
    $(window).scrollTop(0);
})

// 滚动出现右边小点功能
$(window).on("scroll",function(){
    //滚动事件，滚出去的高度兼容代码
    var scrollHight=document.body.scrollTop||window.pageYOffset||document.documentElement.scrollTop;
    if(scrollHight>=800){
        $(".go-top").animate({"right":"10px"},1000); 
    }else{
        $(".go-top").animate({"right":"-50px"},1000);  
    }
})
  //点击回到顶部--右边小点功能
  $(".go-top").on("click",function(){
    $(window).scrollTop(0);
})

//返回
$("h1").on("click",function(){
    history.back();
})
//导航栏横向滚动
var nav_li
function topScroll(){
    var myScroll = new IScroll('.nav-top', { eventPassthrough: true, scrollX: true, scrollY: false, preventDefault: false });
    //点击滚动
    nav_li=document.querySelectorAll(".nav-top li");
    //引用点击事件
    $(".nav-top li").on("tap",function(){
        // 加载是加载弹出层
        $(".layout").show();
        // myScroll.scrollToElement(this);
        selector(this); 
        //获取当前id 重新渲染商品列表
        var id=this.id;
        getGoods(id);
    })
    //排他法
    function selector(dom){
        for(var i=0;i<nav_li.length;i++){
            nav_li[i].children[0].classList.remove("selector");
        }
        dom.children[0].classList.add("selector");
    }
}


//初始化导航栏
getTitle();
function getTitle(){
    $.get("http://193.112.55.79:9090/api/getbaicaijiatitle",function(res){
        // console.log(res);
        var data=res.result;
        var html=template("temptitle",{"data":data});
        $("nav .title").html(html);
        //导航栏横向滚动
        topScroll();
        nav_li[1].children[0].classList.add("selector");
        var id=1;
        getGoods(id);
    },"json");
}
//初始化商品表
function getGoods(id){
    $.get("http://193.112.55.79:9090/api/getbaicaijiaproduct",{"titleid":id},function(res){
        var data=res.result;
        var html=template("tempgoods",{"data":data});   
        $(".bc-goods>ul").html(html);
        // 加载成功后取消弹出层
        $(".layout").hide();
    },"json");
}
}